package com.example.kot

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class SomeUnitTest {

	@Test
	fun sum() {
		assertThat(1 + 1).isEqualTo(2)
	}
}
