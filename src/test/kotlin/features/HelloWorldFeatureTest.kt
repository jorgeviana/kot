package features

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

@Feature
class HelloWorldFeatureTest {

    @Test
    fun `hello world feature`() {
        assertThat("Hello, World!").isEqualTo("Hello, World!")
    }
}
