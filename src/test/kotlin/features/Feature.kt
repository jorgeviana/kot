package features

import org.junit.jupiter.api.Tag

@Tag("Feature")
annotation class Feature

@Tag("Pending")
annotation class Pending